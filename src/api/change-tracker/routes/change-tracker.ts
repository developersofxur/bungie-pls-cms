/**
 * change-tracker router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::change-tracker.change-tracker');
