/**
 * brief controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::brief.brief');
