FROM node:18.12-alpine
RUN apk update && apk add  build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev
ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}
WORKDIR /app/
COPY ./package.json ./package-lock.json ./
RUN npm install
COPY ./ ./
RUN npm run build
EXPOSE 1337
CMD ["npm", "run", "develop"]
