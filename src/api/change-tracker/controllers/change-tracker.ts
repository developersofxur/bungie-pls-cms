/**
 * change-tracker controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::change-tracker.change-tracker');
