/**
 * change-tracker service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::change-tracker.change-tracker');
