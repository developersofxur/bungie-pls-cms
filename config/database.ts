export default ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: "postgres.db",
      port: 5432,
      database: "bungiepls",
      user: "bungiepls_strapi",
      password: env('DATABASE_PASSWORD')
    },
    useNullAsDefault: true,
  },
});
